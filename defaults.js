

function defaults(obj, age = 60) {

    for(let property in obj)
    {
        if(property==='age')
        {
            obj[property] = age;
        }
    }

    return obj;
    
}

module.exports = defaults


function keys(inputObj) {

    const arrOfKeys = [];

    for(let property in inputObj)
    {
        arrOfKeys.push(property);
    }

    return arrOfKeys;
}

module.exports = keys;
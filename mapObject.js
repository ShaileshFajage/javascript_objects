

function mapObject(obj, cb) {

    const resObj = Object.assign({}, obj);
    
    for(let property in obj)
    {
        resObj[property] = cb(obj[property])
    }

    return resObj;
}

function add100(val) {
    
    return val+" 100";
}


module.exports = {
    mapObject,
    add100
}
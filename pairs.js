

function pairs(obj) {
    
    let result = Object.keys(obj).map((key) => [key, obj[key]]);

    return result;
}

module.exports = pairs;